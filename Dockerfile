FROM openjdk:17-oracle as builder

WORKDIR /tmp
COPY . .

RUN chmod +x ./gradlew
RUN ./gradlew assemble

FROM openjdk:17-oracle

WORKDIR /app
COPY --from=builder /tmp/build/libs/java-springboot-template.jar /app

EXPOSE 8080

CMD java -jar /app/java-springboot-template.jar
