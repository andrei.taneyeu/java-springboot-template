CURRENT_DIRECTORY := $(shell pwd)
APP = application #specify your app name here

help:
	@echo "Please use \`make <target>' where <target> is one of:"
	@echo "-----------------------"
	@echo "  start           builds the images and starts the containers"
	@echo "  stop 			 stops all containers"
	@echo "  restart         restarts all containers"
	@echo "  build           builds the images"
	@echo "  check           runs the test suite"
	@echo "  clean           when you really need to start over"
	@echo "  status			 shows running containers"
	@echo "  tails			 shows logs"

start:
	@docker-compose up -d
	@echo "Started development server at http://0.0.0.0:8080/"

stop:
	@docker-compose stop

status:
	@docker-compose ps

restart: stop start

check:
	@./gradlew cleanTest test

clean: stop
	@docker-compose rm --force

build:
	@docker-compose build $(APP) --no-cache

tail:
	@docker-compose logs -f

.PHONY: help start stop status restart check clean build tail
