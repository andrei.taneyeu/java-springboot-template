package io.getwith;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringbootTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSpringbootTemplateApplication.class, args);
    }

}
