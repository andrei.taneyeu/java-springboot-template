package io.getwith.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @GetMapping("/")
    @Operation(description = "Test endpoint with a greeting")
    public String greeting() {
        return ("Hello, World!");
    }

}
