package io.getwith;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class JavaSpringbootTemplateApplicationTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String TEST_SQL = "SELECT 1";

    @Test
    public void greetingShouldReturnDefaultMessage() {
        Assertions.assertTrue((this.restTemplate.getForObject("http://localhost:" + port + "/",
                String.class)).contains("Hello, World"));
    }

    @Test
    public void testContainer() {
        Assertions.assertEquals(1, jdbcTemplate.queryForObject(TEST_SQL, Integer.class));
    }

}

